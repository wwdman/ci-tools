#! /bin/zsh

# godnight.sh 
# @date: 26/07/2020
# @auth: Daniel Stratti
#
# @desc: Script to backup after your day, closes application opened from 
# 		 goodmorning.sh

# # Finally open ticket queue and slack 
# osascript -e 'quit app "Sublime Text"'


ECHO "Good Night!"
