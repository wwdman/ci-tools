# ci-tools
command line tools for speeding up and automating work

## Scripts:
### morning.sh
`morning.sh` is a script to open all applications and websites required for 
daily activities

#### Usage
```sh
morn # opens all aplication listed in morn.sh
```

### night.sh
`night.sh` is a script to close all applications and websites required for 
daily activities

#### Usage
```sh
night # closes all aplication listed in night.sh
```

### doc.py
`doc.py` is a script ot create a file and insert a documentation header with the
file name, date, author (git user) and placeholder tags for the description and 
ticket

#### Instalation:
`dpc.py` uses the following libraries.
- sys
- datetime

Create a symlink in a unix based system from the doc.py file to a 
location on your systems `PATH`
```sh
chmod +x ./doc.py
ln -s  path/to/doc.py dir/on/PATH/doc
```

#### Usage
```sh
doc
#	- Prints the doc manual and documentation
doc ./some/dir/test.sql:
# 	- creates test.sql in the desired location and opens it in your default 
#     editor for .sql files. The header is commented out according to the 
#	  extension.
```
