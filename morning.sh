#! /bin/zsh

# godmorning.sh 
# @date: 25/07/2020
# @auth: Daniel Stratti
#
# @desc: Script to open all required application need to start your day

# Open DB editor and Python IDE IDE
open /Applications/<IDE>.app
open /Applications/<DATA IDE>.app

# API Testing Apps
open /Applications/<API Postman  App>.app
open /Applications/<FTP App>.app
# open /Applications/Postman.app
open -a 'Google Chrome' https://<Company Testing Site>

# Finally open ticket queue and slack 
open -a 'Google Chrome' https://<project management site>
open "/Applications/<Email>.app"
open /Applications/<Messaging App>.app


ECHO "Good Morning\!"
