#! /usr/bin/python
import os
import sys
from datetime import date

help_desc = '''
 doc.py

 @date: 2020-08-28
 @auth: Danie Stratti

 @desc: 
 Create a file and document it with the date and author (git user) in a header.

 @usage:
	doc <filename>
	filename: The name and extension of the fiule to create

 @examples:
 	doc ./some/dir/test.sql:
 		- creates test.sql in the desired location and opens it in your default 
 		  editor for .sql files. The header is commented out according to the 
 		  extension.

 	/**
	 test.sql

	 @ticket:
	 @date: 2020-09-13
	 @auth: Danie Stratti < daniel.stratti@alayacare.com >

	 @desc: 
	*/

 @setup:
 - Create a symlink in a unix based system from this file to a location on your 
   PATH
 	chmod +x ./doc.py
 	ln -s  path/to/doc.py dir/on/PATH/doc
'''


## Dictionary to map file extensions to comment characters
COMMENT = {
	'py': ["'''", "", "'''"],
	'sh': ['#', '#', '#'],
	'r': ['#', '#', '#'],
	'txt': ['/**', '*', '*/'],
	'sql': ['/**', '', '*/'],
	'php': ["<?php\n/**", '', '*/'],
}


def print_help():

	print(''.join(["-" for i in range(80)]))
	print(help_desc)
	print(''.join(["-" for i in range(80)]))


'''
Create the header documentation for a file. this should include the file name,
date created, author and a place heading for a description

@param file {String}: the name of the file to document
@return {String}: the formatted header ready to be inserted into a file
'''
def header(file):
	header = ""
	name = ""
	comment_sym = get_comment(file)
	

	# Get git user details
	stream = os.popen("git config user.name")
	name += stream.read().strip()

	stream = os.popen("git config user.email")
	name += " < {e} >".format(e = stream.read().strip())
	

	# Create documentation header
	header = "{sc}\n{mc} {file}\n{mc}\n{mc} @ticket:\t\n".format(
										sc = comment_sym[0], 
										mc = comment_sym[1], 
										file = file)
	header += "{mc} @date:\t\t{date}\n".format(
										mc = comment_sym[1], 
										date = date.today())
	header += "{mc} @auth:\t\t{user}\n{mc}\n{mc} @desc: \n{ec}".format(
										mc = comment_sym[1],
										user = name,
										ec = comment_sym[2],)
	return header


'''
Determine the appropriate characters for multi-line comments based of the file 
extension

@param file {String}: the name of the file to document
@return {List}: An array of size 3 containing the start, middle and closing 
				comment characters
'''
def get_comment(file):
	comment_sym = []
	
	# Based off file extension get comment and executable
	ext = file.split('.').pop()
	return COMMENT.get(ext, ['##','##','##'])


'''
Script to create a new file with header documentation based on the logged in git 
user. and working directory

Example

#! < --path -p /bin/bash >

# @date: 25/07/2020
# @auth: Daniel Stratti
#
# @desc:
'''
if __name__ == '__main__':

	# Get file and path
	file = sys.argv[1] if len(sys.argv) > 1 else None
	wd = os.path.abspath(os.getcwd())
	loc = "%s/%s" % (wd, file)

	if file is not None:

		# Create header doc
		header = header(file)

		# create file and add heaer
		f = open(loc, 'w+')
		f.write(header)
		f.close()

		os.system("open %s" % loc)
	else:

		print_help()
